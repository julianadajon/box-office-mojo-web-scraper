# Juliana Dajon
# Fall 2017
# RA Qi
# This file scrapes box office WEEKEND data by countries from Box Office Mojo.

from bs4 import BeautifulSoup
import csv
import requests
import unicodedata

debug = False 

def get_data(link, master) :
    try: 
        content = requests.get(link).content
        soup = BeautifulSoup(content, 'html.parser')    
        
        #for japan 2017
        table = soup.findAll('table')[4]
        page_data = table.findAll("tr")    
        page_data = scrape(page_data, link, master)
        
        return page_data
    
    except requests.exceptions.ChunkedEncodingError: 
        print("error in function get_data")
        print("ChunkedEncodingError; check file for data scraped thus far.")
        path = "/Users/julianadajon/Documents/School/2017_Fall/RA Qi Personal/weekendBoxOffice/chunkedEncodingError"
        with open(path, "w", encoding = 'utf8') as f :
            writer = csv.writer(f) 
            writer.writerows(master)
            


def get_wkd_date(link, master):
    try: 
        content = requests.get(link).content
        soup = BeautifulSoup(content, 'html.parser')  
        
        title = soup.findAll("title")
        return title[0]
    
    except requests.exceptions.ChunkedEncodingError:
        print("error in function get_wkd_date")
        print("ChunkedEncodingError; check file for data scraped thus far.")
        path = "/Users/julianadajon/Documents/School/2017_Fall/RA Qi Personal/weekendBoxOffice/chunkedEncodingError"
        with open(path, "w", encoding = 'utf8') as f :
            writer = csv.writer(f) 
            writer.writerows(master)        


def scrape(data, link, master) : 
    obs = []
    c = 0
    data = data[:-1]
    for tr in data :
        
        
        if c>1 :
            
            td_list = tr.findAll('td')
            movie=[]
            
            for td in td_list :
                movie.append(td)
                
            weekend_date = get_wkd_date(link, master)
            movie.append(weekend_date)
            obs.append(movie)

        c+=1
        
    return obs


def clean(obs_list , country) :
    
    new_obs = []
    
    for obs in obs_list :
        new_charac = []
        
        for charac in obs: 
            
            #print("charac: ", charac)
            
            try :
                c = (charac.find('font')).get_text()
                #print(c)    
                
            #handling date
            except (AttributeError) :
                c = charac.get_text()
                c = str(c).replace("\x96", '-')
                if country == "japan" or country == "china":
                    c = c[18:]
                if country == "uk": 
                    c = c[27:]
 
            new_charac.append(c)
        new_obs.append(new_charac)
    
    return new_obs

def country(c, master) :
    
    #years 2002-2017
    #finished years: 
    # 2017,2016,2015,14,13,12,11,10,9,8,7,6,5
    if c == "japan" :
        link = "http://www.boxofficemojo.com/intl/"+c+"/"
        master = get_wknds(link, "2005", master)
        

    #link years: 2017 | 2016 | 2015 | 2014 | 2013 | 2011 | 2010 | 2009 | 2008 | 2007 
    #finished years: 
    # 2017, 16,15, 14,13,11,10,09,08,07
    if c == "china": 
        link = "http://www.boxofficemojo.com/intl/"+c+"/"
        master = get_wknds(link, "2007", master)      
        
        
    
    #2017 | 2016 | 2015 | 2014 | 2013 | 2012 | 2011 | 2010 | 2009 | 2008 | 2007 | 2006 | 2005
    #finished years
    # 2017, 2016, 15, 14 , 13, 12, 11,10, 09, 8, 7, 6
    if c == "uk": 
        link = "http://www.boxofficemojo.com/intl/"+c+"/"
        master = get_wknds(link, "2017", master, c)     
        
        
    #2017 | 2016 | 2015 | 2014 | 2013 | 2012 | 2011 | 2010 | 2009 | 2008 | 2007 | 2006 | 2005 | 2004 | 2003 | 2002 | 2001
    #finished years
    # 
    if c == "hongkong":
        link = "http://www.boxofficemojo.com/intl/"+c+"/"
        #change year 
        master = get_wknds(link, "2017", master, c)       
        
        
        
        
    return master
    

def get_wknds(link, year, master, c) :

    yr_link = link + "?yr="+ year +"&p=.htm"
    wknd_num = get_wknd_nums(yr_link)
    print("wknd_num: ", wknd_num)
    for num in wknd_num :
        
        w_link = link + "?yr="+ year +"&wk=" + num + "&p=.htm"
        obs = get_data(w_link, master)
        obs = clean(obs, c)
    
        for i in obs: 
            print(i)
            master.append(i)
        
    return master

def get_wknd_nums(link):
    
    content = requests.get(link).content
    soup = BeautifulSoup(content, 'html.parser')    
        
    table = soup.findAll('table')[2]
    href = table.findAll('a', href=True)


    week_num = []
    for a in href: 
        a_str = str(a['href'])
        if a_str[0] == '.' :

            #if weekend num is double digits
            if len(a_str) == 23 :
                week_num.append( a_str[14]+a_str[15] )
            if len(a_str) == 22 :
                week_num.append( a_str[14] )
    
    
    return week_num


if __name__ == '__main__' :
    
    #must use correctly formatted country name
    #japan | china | uk | hongkong
    c = "uk"
    master = []
    master = country(c, master)
    
    #testing final list
    for i in master: 
        print("testing master: ", i)

    #write to csv
    #use 'w' to write new file
    #use 'a' to append to existing file
    path = "/Users/julianadajon/Documents/School/2017_Fall/RA Qi Personal/weekendBoxOffice/" + c + "wkndBoxOffice.csv"
    with open(path, "a", encoding = 'utf8') as f :
        writer = csv.writer(f) 
        writer.writerows(master)    
