# Juliana Dajon
# Fall 2017
# RA Qi
# This file scrapes box office data by countries from Box Office Mojo.

from bs4 import BeautifulSoup
import csv
import requests
import unicodedata

debug = False 

#extract movie links from Box Office Mojo for domestic top 100
#and return as list
def get_data(link, obs, year, country) :
    
    content = requests.get(link).content
    soup = BeautifulSoup(content, 'html.parser')    
        
    table = soup.findAll('table')[4]
    movie_info = table.findAll("tr")
    
    if country == "hongkong" :
        specialYears = ["2002"]
        obs = scrape( specialYears, year, obs, movie_info )
        
    if country == "uk" :
        specialYears = ["2002", "2003"]
        obs = scrape( specialYears, year, obs, movie_info )        
    
    if country == "australia": 
        specialYears = ["2002", "2003", "2001"]
        obs = scrape( specialYears, year, obs, movie_info )        

    if country == "france": 
        specialYears = ["2002", "2003"]
        obs = scrape( specialYears, year, obs, movie_info )        
    
    if country == "germany": 
        specialYears = ["2003", "2002", "2001"]
        obs = scrape( specialYears, year, obs, movie_info )        
        
    if country == "italy": 
        specialYears = []
        obs = scrape( specialYears, year, obs, movie_info )   
        
    if country == "spain": 
        specialYears = ["2002", "2003"]
        obs = scrape( specialYears, year, obs, movie_info ) 
        
    if country == "turkey": 
        specialYears = []
        obs = scrape( specialYears, year, obs, movie_info )   
        
    if country == "japan": 
        specialYears = ["2002", "2003"]
        obs = scrape( specialYears, year, obs, movie_info )  
        
    if country == "korea": 
        specialYears = ["2015", "2016", "2009", "2008"]
        obs = scrape( specialYears, year, obs, movie_info ) 
        
    if country == "taiwan": 
        specialYears = []
        obs = scrape( specialYears, year, obs, movie_info )  
    
    if country == "brazil": 
        specialYears = ["2002", "2003", "2012"]
        obs = scrape( specialYears, year, obs, movie_info )    
    
    if country == "mexico": 
        specialYears = ["2002", "2003" ]
        obs = scrape( specialYears, year, obs, movie_info )     
    
    if country == "india": 
        specialYears = ["2008", "2007"]
        obs = scrape( specialYears, year, obs, movie_info )   
    
    #russia
    if country == "cis": 
        specialYears = []
        obs = scrape( specialYears, year, obs, movie_info )   
    
    if country == "china": 
        specialYears = ["2016", "2017", "2009"]
        obs = scrape( specialYears, year, obs, movie_info )       
    
    return obs
    

# this function scrapes the data for each observation and 
# handles special cases (if a characteristic does not exist, like distr, 
# for specific year or subset of a year)
def scrape(specialYears, year, obs, movie_info):
    
    if year in specialYears :
        
        distrFlag = 0
        c = 0
        for tr in movie_info: 
            
            if c >= 1 :
                movie = []
                
                rank = tr.find_next('font')
                title = rank.find_next('font')
                
                string_title_next = str(title.find_next('font'))
                
                print(title, year, rank)
                print("title.find_next: ", title.find_next('font'))
                
                 
                #distributor column exists
                if string_title_next == r'<font size="2">n/a</font>' or distrFlag == 1:

                    print("in if title.find_next('font')") 
                    print( " ") 
                
                    
                    distr = title.find_next('font')
                    gross = distr.find_next('font')
                    release = gross.find_next('font')
                    
                    #checking if distribution column exists
                    distrFlag = 1
                
                    
                #distributor column does not exist
                else:
                    
                    print("in else stmt") 
                    print( " ") 
                    distr = 'n/a'
                    gross = title.find_next('font')
                    release = gross.find_next('font')                        
                
                movie.append(rank) 
                movie.append(title)
                movie.append(distr)
                movie.append(gross)
                movie.append(release)
                
                
                obs.append(movie)
                
        
                
                
            c+=1         
    
    else :
        c = 0
        for tr in movie_info: 
            
            if c >= 1 :
                movie = []
                
                rank = tr.find_next('font')
                title = rank.find_next('font')
                distr = title.find_next('font')
                gross = distr.find_next('font')
                release = gross.find_next('font')
                
                movie.append(rank) 
                movie.append(title)
                movie.append(distr)
                movie.append(gross)
                movie.append(release)
                
             
                obs.append(movie)
                
            c+=1                
        
    return obs



#cleans list of data, extracts 
def clean(data, year) :

    #iterate through each observation, then through each characteristic 
    #strip tags
    new_obs = []
    
    for obs in data: 
        new_charac = []
        for charac in obs: 
            if charac == "n/a" : 
                c = charac 
                new_charac.append(c)
                continue
            else: 
                c = str(charac)
                c = c[15:]
                c = c[ : (len(charac)-1) - 7]          
                #handling gross (slightly different tag)
                if c[0:3] == '<b>' :
                    c = c.strip("</b>")
                    c = c.replace("," , "") 
                    c = c.replace("$", "") 
                if debug: print(c)
##                c = c.decode('utf-8').encode('ascii', 'ignore')
                new_charac.append(c) 
        new_charac.append(year)
        new_obs.append(new_charac)
            
    return new_obs


def get_obs_by_year(pageStart, pageEnd, master, country, year) :
    
    for i in range(pageStart, pageEnd):
        
        obs = []
        i = str(i)  
        link = "http://www.boxofficemojo.com/intl/" + country + "/yearly/?yr=" + year + "&sort=gross&order=DESC&pagenum=" + i + "&p=.htm"
        m = get_data(link, obs, year, country) 
        m = clean(m, year)
        
        #appends cleaned movie observation to master list
        for movie in m: 
            
            master.append(movie)    
    
    return master

def country( country, master ) :
    
    if country == "hongkong" :
        
        master = get_obs_by_year(1 , 3, master, country, "2016")
        master = get_obs_by_year(1 , 5, master, country, "2015")
        master = get_obs_by_year(1 , 5, master, country, "2014")
        master = get_obs_by_year(1 , 4, master, country, "2013")
        master = get_obs_by_year(1 , 4, master, country, "2012")
        master = get_obs_by_year(1 , 4, master, country, "2011")
        master = get_obs_by_year(1 , 4, master, country, "2010")
        master = get_obs_by_year(1 , 4, master, country, "2009")
        master = get_obs_by_year(1 , 4, master, country, "2008")
        master = get_obs_by_year(1 , 4, master, country, "2007")
        master = get_obs_by_year(1 , 3, master, country, "2006")
        master = get_obs_by_year(1 , 3, master, country, "2005")
        master = get_obs_by_year(1 , 3, master, country, "2004")
        master = get_obs_by_year(1 , 3, master, country, "2003")
        master = get_obs_by_year(1 , 3, master, country, "2002")        
        
    if country == "uk" :
        
        master = get_obs_by_year(1 , 7, master, country, "2017")
        master = get_obs_by_year(1 , 9, master, country, "2016")
        master = get_obs_by_year(1 , 5, master, country, "2015")
        master = get_obs_by_year(1 , 5, master, country, "2014")
        
        master = get_obs_by_year(1 , 6, master, country, "2013")
        master = get_obs_by_year(1 , 7, master, country, "2012")
        master = get_obs_by_year(1 , 7, master, country, "2011")
        master = get_obs_by_year(1 , 4, master, country, "2010")
        
        master = get_obs_by_year(1 , 5, master, country, "2009")
        master = get_obs_by_year(1 , 7, master, country, "2008")
        master = get_obs_by_year(1 , 6, master, country, "2007")
        master = get_obs_by_year(1 , 6, master, country, "2006")
        
        master = get_obs_by_year(1 , 4, master, country, "2005")
        master = get_obs_by_year(1 , 4, master, country, "2004")
        master = get_obs_by_year(1 , 4, master, country, "2003")
        master = get_obs_by_year(1 , 3, master, country, "2002")
        
    if country == "australia":
        
        master = get_obs_by_year(1 , 5, master, country, "2017")
        master = get_obs_by_year(1 , 4, master, country, "2016")
        master = get_obs_by_year(1 , 4, master, country, "2015")
        master = get_obs_by_year(1 , 4, master, country, "2014")
        
        master = get_obs_by_year(1 , 4, master, country, "2013")
        master = get_obs_by_year(1 , 4, master, country, "2012")
        master = get_obs_by_year(1 , 4, master, country, "2011")
        master = get_obs_by_year(1 , 4, master, country, "2010")
        
        master = get_obs_by_year(1 , 5, master, country, "2009")
        master = get_obs_by_year(1 , 4, master, country, "2008")
        master = get_obs_by_year(1 , 4, master, country, "2007")
        master = get_obs_by_year(1 , 4, master, country, "2006")
        
        master = get_obs_by_year(1 , 4, master, country, "2005")
        master = get_obs_by_year(1 , 4, master, country, "2004")
        master = get_obs_by_year(1 , 4, master, country, "2003")
        master = get_obs_by_year(1 , 4, master, country, "2002")
        master = get_obs_by_year(1 , 3, master, country, "2001")
        
    
    if country == "france": 
        master = get_obs_by_year(1 , 5, master, country, "2017")
        master = get_obs_by_year(1 , 5, master, country, "2016")
        master = get_obs_by_year(1 , 4, master, country, "2015")
        master = get_obs_by_year(1 , 5, master, country, "2014")
        
        master = get_obs_by_year(1 , 5, master, country, "2013")
        master = get_obs_by_year(1 , 4, master, country, "2012")
        master = get_obs_by_year(1 , 5, master, country, "2011")
        master = get_obs_by_year(1 , 5, master, country, "2010")
        
        master = get_obs_by_year(1 , 7, master, country, "2009")
        master = get_obs_by_year(1 , 8, master, country, "2008")
        master = get_obs_by_year(1 , 5, master, country, "2007")
        master = get_obs_by_year(1 , 4, master, country, "2006")
        
        master = get_obs_by_year(1 , 4, master, country, "2005")
        master = get_obs_by_year(1 , 4, master, country, "2004")
        master = get_obs_by_year(1 , 7, master, country, "2003")
        master = get_obs_by_year(1 , 7, master, country, "2002")
               
    if country == "germany": 
        master = get_obs_by_year(1 , 3, master, country, "2017")
        master = get_obs_by_year(1 , 4, master, country, "2016")
        master = get_obs_by_year(1 , 4, master, country, "2015")
        master = get_obs_by_year(1 , 5, master, country, "2014")
        
        master = get_obs_by_year(1 , 5, master, country, "2013")
        master = get_obs_by_year(1 , 5, master, country, "2012")
        master = get_obs_by_year(1 , 5, master, country, "2011")
        master = get_obs_by_year(1 , 4, master, country, "2010")
        
        master = get_obs_by_year(1 , 5, master, country, "2009")
        master = get_obs_by_year(1 , 5, master, country, "2008")
        master = get_obs_by_year(1 , 5, master, country, "2007")
        master = get_obs_by_year(1 , 4, master, country, "2006")
        
        master = get_obs_by_year(1 , 4, master, country, "2005")
        master = get_obs_by_year(1 , 4, master, country, "2004")
        master = get_obs_by_year(1 , 4, master, country, "2003")
        master = get_obs_by_year(1 , 4, master, country, "2002")
        master = get_obs_by_year(1 , 3, master, country, "2001")
        
        
    if country == "italy": 
        master = get_obs_by_year(1 , 4, master, country, "2017")
        master = get_obs_by_year(1 , 5, master, country, "2016")
        master = get_obs_by_year(1 , 4, master, country, "2015")
        master = get_obs_by_year(1 , 4, master, country, "2014")
        
        master = get_obs_by_year(1 , 4, master, country, "2013")
        master = get_obs_by_year(1 , 5, master, country, "2012")
        master = get_obs_by_year(1 , 5, master, country, "2011")
        master = get_obs_by_year(1 , 4, master, country, "2010")
        
        master = get_obs_by_year(1 , 5, master, country, "2009")
        master = get_obs_by_year(1 , 5, master, country, "2008")
        master = get_obs_by_year(1 , 5, master, country, "2007")
        master = get_obs_by_year(1 , 4, master, country, "2006")
        
        master = get_obs_by_year(1 , 4, master, country, "2005")
        master = get_obs_by_year(1 , 4, master, country, "2004")
        master = get_obs_by_year(1 , 4, master, country, "2003")
        master = get_obs_by_year(1 , 4, master, country, "2002")
    
    
    if country == "spain": 
        
        master = get_obs_by_year(1 , 5, master, country, "2017")
        master = get_obs_by_year(1 , 6, master, country, "2016")
        master = get_obs_by_year(1 , 6, master, country, "2015")
        master = get_obs_by_year(1 , 5, master, country, "2014")
        
        master = get_obs_by_year(1 , 6, master, country, "2013")
        master = get_obs_by_year(1 , 5, master, country, "2012")
        master = get_obs_by_year(1 , 6, master, country, "2011")
        master = get_obs_by_year(1 , 5, master, country, "2010")
        
        master = get_obs_by_year(1 , 5, master, country, "2009")
        master = get_obs_by_year(1 , 5, master, country, "2008")
        master = get_obs_by_year(1 , 6, master, country, "2007")
        master = get_obs_by_year(1 , 5, master, country, "2006")
        
        master = get_obs_by_year(1 , 4, master, country, "2005")
        master = get_obs_by_year(1 , 5, master, country, "2004")
        master = get_obs_by_year(1 , 5, master, country, "2003")
        master = get_obs_by_year(1 , 5, master, country, "2002")
        
        
    if country == "turkey": 
        master = get_obs_by_year(1 , 5, master, country, "2017")
        master = get_obs_by_year(1 , 5, master, country, "2016")
        master = get_obs_by_year(1 , 5, master, country, "2015")
        master = get_obs_by_year(1 , 4, master, country, "2014")
        
        master = get_obs_by_year(1 , 5, master, country, "2013")
        master = get_obs_by_year(1 , 4, master, country, "2012")
        master = get_obs_by_year(1 , 4, master, country, "2011")
        master = get_obs_by_year(1 , 4, master, country, "2010")
        
        master = get_obs_by_year(1 , 4, master, country, "2009")
        master = get_obs_by_year(1 , 4, master, country, "2008")
        master = get_obs_by_year(1 , 4, master, country, "2007")

        
    if country == "japan": 
        master = get_obs_by_year(1 , 3, master, country, "2017")
        master = get_obs_by_year(1 , 3, master, country, "2016")
        master = get_obs_by_year(1 , 3, master, country, "2015")
        master = get_obs_by_year(1 , 3, master, country, "2014")
        
        master = get_obs_by_year(1 , 3, master, country, "2013")
        master = get_obs_by_year(1 , 3, master, country, "2012")
        master = get_obs_by_year(1 , 3, master, country, "2011")
        master = get_obs_by_year(1 , 3, master, country, "2010")
        
        master = get_obs_by_year(1 , 3, master, country, "2009")
        master = get_obs_by_year(1 , 4, master, country, "2008")
        master = get_obs_by_year(1 , 4, master, country, "2007")
        master = get_obs_by_year(1 , 3, master, country, "2006")
        
        master = get_obs_by_year(1 , 3, master, country, "2005")
        master = get_obs_by_year(1 , 3, master, country, "2004")
        master = get_obs_by_year(1 , 3, master, country, "2003")
        master = get_obs_by_year(1 , 3, master, country, "2002")   
            
    if country == "korea": 
        master = get_obs_by_year(1 , 5, master, country, "2017")
        master = get_obs_by_year(1 , 6, master, country, "2016")
        master = get_obs_by_year(1 , 4, master, country, "2015")
        master = get_obs_by_year(1 , 3, master, country, "2014")
        
        master = get_obs_by_year(1 , 3, master, country, "2013")
        master = get_obs_by_year(1 , 3, master, country, "2012")
        master = get_obs_by_year(1 , 7, master, country, "2011")
        master = get_obs_by_year(1 , 5, master, country, "2010")
        
        master = get_obs_by_year(1 , 5, master, country, "2009")
        master = get_obs_by_year(1 , 6, master, country, "2008")
        master = get_obs_by_year(1 , 6, master, country, "2007")

    if country == "taiwan": 
        master = get_obs_by_year(1 , 2, master, country, "2017")
        master = get_obs_by_year(1 , 3, master, country, "2011")
        master = get_obs_by_year(1 , 4, master, country, "2010")
        master = get_obs_by_year(1 , 4, master, country, "2009")    
        master = get_obs_by_year(1 , 5, master, country, "2008") 
        
        
    if country == "brazil": 
        master = get_obs_by_year(1 , 3, master, country, "2017")
        master = get_obs_by_year(1 , 4, master, country, "2016")
        master = get_obs_by_year(1 , 4, master, country, "2015")
        master = get_obs_by_year(1 , 5, master, country, "2014")
        
        master = get_obs_by_year(1 , 5, master, country, "2013")
        master = get_obs_by_year(1 , 5, master, country, "2012")
        master = get_obs_by_year(1 , 4, master, country, "2011")
        master = get_obs_by_year(1 , 4, master, country, "2010")
        
        master = get_obs_by_year(1 , 4, master, country, "2009")
        master = get_obs_by_year(1 , 5, master, country, "2008")
        master = get_obs_by_year(1 , 4, master, country, "2007")
        master = get_obs_by_year(1 , 3, master, country, "2006")
        
        master = get_obs_by_year(1 , 3, master, country, "2005")
        master = get_obs_by_year(1 , 2, master, country, "2004")
        master = get_obs_by_year(1 , 2, master, country, "2003")
        master = get_obs_by_year(1 , 3, master, country, "2002")      
    
    if country == "mexico": 
        master = get_obs_by_year(1 , 3, master, country, "2017")
        master = get_obs_by_year(1 , 4, master, country, "2016")
        master = get_obs_by_year(1 , 4, master, country, "2015")
        master = get_obs_by_year(1 , 4, master, country, "2014")
        
        master = get_obs_by_year(1 , 4, master, country, "2013")
        master = get_obs_by_year(1 , 4, master, country, "2012")
        master = get_obs_by_year(1 , 4, master, country, "2011")
        master = get_obs_by_year(1 , 4, master, country, "2010")
        
        master = get_obs_by_year(1 , 4, master, country, "2009")
        master = get_obs_by_year(1 , 5, master, country, "2008")
        master = get_obs_by_year(1 , 5, master, country, "2007")
       
        master = get_obs_by_year(1 , 4, master, country, "2004")
        master = get_obs_by_year(1 , 4, master, country, "2003")
        master = get_obs_by_year(1 , 4, master, country, "2002")    
    
    if country == "india": 
        master = get_obs_by_year(1 , 2, master, country, "2009")
        master = get_obs_by_year(1 , 3, master, country, "2008")
        master = get_obs_by_year(1 , 3, master, country, "2007")      
    #russia
    if country == "cis": 
        master = get_obs_by_year(1 , 5, master, country, "2017")
        master = get_obs_by_year(1 , 6, master, country, "2016")
        master = get_obs_by_year(1 , 4, master, country, "2015")
        master = get_obs_by_year(1 , 4, master, country, "2014")
        
        master = get_obs_by_year(1 , 6, master, country, "2013")
        master = get_obs_by_year(1 , 5, master, country, "2012")
        master = get_obs_by_year(1 , 5, master, country, "2011")
        master = get_obs_by_year(1 , 5, master, country, "2010")
        
        master = get_obs_by_year(1 , 5, master, country, "2009")
        master = get_obs_by_year(1 , 5, master, country, "2008")
        master = get_obs_by_year(1 , 5, master, country, "2007")
        master = get_obs_by_year(1 , 4, master, country, "2006")
        
        master = get_obs_by_year(1 , 3, master, country, "2005")
        master = get_obs_by_year(1 , 3, master, country, "2004")       
 
 
    if country == "china": 
        master = get_obs_by_year(1 , 5, master, country, "2017")
        master = get_obs_by_year(1 , 5, master, country, "2016")
        master = get_obs_by_year(1 , 3, master, country, "2015")
        master = get_obs_by_year(1 , 3, master, country, "2014")
        
        master = get_obs_by_year(1 , 3, master, country, "2013")

        
        master = get_obs_by_year(1 , 2, master, country, "2009")
        master = get_obs_by_year(1 , 3, master, country, "2008")
        master = get_obs_by_year(1 , 3, master, country, "2007")
    
     
 
    
        
    return master

#Main Code:
if __name__ == '__main__' :
    
    master = []
    
    
    
    c = "china"
    master = country(c, master) 

    
    for i in master :
        print(i)   

   
    #write to csv
    
    path = "/Users/julianadajon/Documents/School/2017 Fall/RA Qi Personal/boxOfficeCountries/" + c + "YrBoxOffice.csv"
    
    with open(path, "w", encoding = 'utf8') as f :
        writer = csv.writer(f) 
        writer.writerows(master)
    
 
    
    


    